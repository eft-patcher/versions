import { JSDOM } from "jsdom";
import fs from 'fs';

/**
 * @type { [key: string]: { nextVer: string | null }}
 */
const versions = [];

const docText = await (await fetch('https://escapefromtarkov.fandom.com/wiki/Changelog')).text();
const { document } = new JSDOM(docText).window;

// Get versions text, assume sorted newer to older
const allVersionsText = [...document.querySelectorAll('#toc .toclevel-1 > a > .toctext')];

allVersionsText.forEach((versionText) => {
    try {
        const version = versionText.textContent.match(/(\d+(?:\.\d+)+)/)[0];
        versions.push(version);
    }
    catch { }
});

fs.writeFileSync('versions.json', JSON.stringify(versions));
